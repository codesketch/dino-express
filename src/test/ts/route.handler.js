//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

const { assert } = require('chai')
const { RouteHandler } = require('../../main/ts/RouteHandler')
const { describe, it } = require('mocha')

describe.skip('Route Handler', () => {
  const api = {
    parameters: [
      { in: 'query', name: 'q', default: 'default search' },
      { in: 'path', name: 'id' },
      { in: 'header', name: 'content-type' }
    ]
  }
  const testObj = new RouteHandler(null, api, null, {
    getOrDefault: (_v, d) => d
  })

  it('collects all parameters', () => {
    const req = {
      params: { id: 23 },
      query: { q: 'text to search' },
      headers: { 'content-type': 'application/json' }
    }
    let actual = testObj.collectParameters(req)
    assert.deepEqual(actual, { id: 23, q: 'text to search', 'content-type': 'application/json' })
  })

  it('collects all parameters with default', () => {
    const req = {
      params: { id: 23 },
      query: {},
      headers: { 'content-type': 'application/json' }
    }
    let actual = testObj.collectParameters(req)
    assert.deepEqual(actual, { id: 23, q: 'default search', 'content-type': 'application/json' })
  })

  it('does not handle if reduce function is not present', () => {
    const api = { parameters: {} }
    const testObj = new RouteHandler(null, api, null, {
      getOrDefault: (_v, d) => d
    })
    const req = {
      params: { id: 23 },
      query: {},
      headers: { 'content-type': 'application/json' }
    }
    let actual = testObj.collectParameters(req)
    assert.deepEqual(actual, [])
  })
})
