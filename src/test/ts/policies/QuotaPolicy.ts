import { describe, it } from 'mocha'
import sinon from 'sinon'
import { QuotaPolicy } from '../../../main/ts/policies/QuotaPolicy'
import { Request } from 'express'
import { assert } from 'chai'

describe('Quota Policy', () => {
  const testObj = new QuotaPolicy()
  const stub = sinon.stub(Request)
  stub['headers'] = { 'x-forwarded-for': '127.0.0.1' }
  it('allows request inside the assigned window', () => {
    testObj.configure({ resetInterval: 10000, window: 1000, allow: 2 })
    // execute
    let actual = testObj.apply(stub as unknown as Request)
    // assert
    assert.equal(actual, true)
  })

  it('blocks request exceeding allowed inside the assigned window', () => {
    testObj.configure({ resetInterval: 10000, window: 1000, allow: 3 })
    for (let i = 0; i < 3; i++) {
      // execute
      let actual = testObj.apply(stub as unknown as Request)
      // assert
      i < 2 ? assert.equal(actual, true, 'should have been allowed') : assert.equal(actual, false, 'should have been blocked')
    }
    // clearInterval(testObj.cleanUpJob)
  })
})
