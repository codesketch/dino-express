//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

const { describe } = require('mocha');
const { ApplicationStateMonitor } = require('../../../main/ts/monitoring/application.state.monitor');
const { assert } = require('chai');
const { State } = require('dino-core');

describe.skip('Application State Monitor', () => {
  it('Returns default state monitoring if no monitors are defined', () => {
    let applicationContext = {
      resolveAll: () => {
        return null;
      },
    };
    let res = new Response();
    ApplicationStateMonitor.middleware(applicationContext)(null, res, null);
    // assert
    assert.equal(res.code, 200);
    assert.equal(res.ct, 'application/json');
    assert.equal(res.out, JSON.stringify({ default: [{ name: 'status', value: 'UP' }] }));
  });

  it('Returns user defined state monitoring if monitors are defined', () => {
    let applicationContext = {
      resolveAll: () => {
        return [new Monitor()];
      },
    };
    let res = new Response();
    ApplicationStateMonitor.middleware(applicationContext)(null, res, null);
    // assert
    assert.equal(res.code, 200);
    assert.equal(res.ct, 'application/json');
    assert.equal(
      res.out,
      JSON.stringify({
        'test-monitor': [
          { name: 'DB-Type', value: 'Mongo' },
          { name: 'DB-State', value: 'CONNECTED' },
        ],
      }),
    );
  });
});

class Response {
  status(code) {
    this.code = code;
    return this;
  }
  type(ct) {
    this.ct = ct;
    return this;
  }
  send(out) {
    this.out = out;
    return this;
  }
}

class Monitor {
  name() {
    return 'test-monitor';
  }

  execute() {
    return State.create().setProperty('DB-Type', 'Mongo').setProperty('DB-State', 'CONNECTED');
  }
}
