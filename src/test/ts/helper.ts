//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { expect, assert } from 'chai'
import { Helper } from '../../main/ts/Helper'
import { AfterHandlerMiddleware, Response } from '../../main/ts/main'
import { BeforeHandlerMiddleware } from '../../main/ts/main'
import { Request, NextFunction } from 'express'

describe('Helper', () => {
  it('normalize path', () => {
    let actual = Helper.normalizePath('prefix/{id}')
    assert.equal(actual, 'prefix/:id')
  })
  it('normalize path with double parameter', () => {
    let actual = Helper.normalizePath('prefix/{id}/another/{param}')
    assert.equal(actual, 'prefix/:id/another/:param')
  })

  it('get a variable using : notation', () => {
    let env = new Map()
    env.set('a:b', 'value')
    let actual = Helper.getVariable(env, 'a:b', 'default')
    // assert
    assert.equal(actual, 'value')
  })

  it('get a variable using _ notation', () => {
    let env = new Map()
    env.set('a_b', 'value')
    let actual = Helper.getVariable(env, 'a:b', 'default')
    // assert
    assert.equal(actual, 'value')
  })

  it('returns default value', () => {
    let env = new Map()
    env.set('a_b', 'value')
    let actual = Helper.getVariable(env, 'c:d', 'default')
    // assert
    assert.equal(actual, 'default')
  })

  it('ensures value is returned', () => {
    assert.equal('abc', Helper.ensureValue('abc', 'def'))
  })

  it('ensures default value is returned', () => {
    assert.equal('def', Helper.ensureValue(undefined, 'def'))
  })

  it('correctly evaluate instance type', () => {
    class AfterHandlerTest extends AfterHandlerMiddleware {
      public handle(_req: Request, _res: Response, _next: NextFunction): void {
        throw new Error('Method not implemented.')
      }
    }
    const afterHandlerTest = new AfterHandlerTest()
    assert.ok(Helper.instanceOf(afterHandlerTest, AfterHandlerMiddleware))
  })

  it('correctly evaluate not instance of type', () => {
    class BeforeHandlerMiddlewareTest extends BeforeHandlerMiddleware {
      public handle(_req: Request, _res: Response, _next: NextFunction): void {
        throw new Error('Method not implemented.')
      }
    }
    const beforeHandlerTest = new BeforeHandlerMiddlewareTest()
    assert.ok(!Helper.instanceOf(beforeHandlerTest, AfterHandlerMiddleware))
  })

  it('validate not base64 encoded', () => {
    const actual = Helper.isBase64('abcdefghi')
    expect(actual).to.be.false
  })

  it('validate base64 encoded', () => {
    const actual = Helper.isBase64('SGVsbG8sIFdvcmxkIQ==')
    expect(actual).to.be.true
  })

  it('decode base64 string', () => {
    const actual = Helper.decodeFromBase64AndParse('SGVsbG8sIFdvcmxkIQ==')
    expect(actual).to.be.eq('Hello, World!')
  })
})
