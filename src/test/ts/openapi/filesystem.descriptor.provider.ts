//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { assert } from 'chai'
import { FilesystemDescriptorProvider } from '../../../main/ts/openapi/filesystem.descriptor.provider'
import { EnvironmentConfiguration } from 'dino-core'

describe('FilesystemDescriptorProvider', () => {
  it('Load default configuration if variable not defined', () => {
    let environment = {
      get: (_k) => {
        return undefined
      },
      getOrDefault: (_k, d) => {
        return d
      }
    } as EnvironmentConfiguration
    let testObj = new FilesystemDescriptorProvider(environment)
    // act
    let actual = testObj.provide()
    // assert
    assert.equal(actual, 'src/main/resources/routes.yml')
  })

  it('Load configuration from : variable', () => {
    let environment: EnvironmentConfiguration = {
      get: (_k) => {
        return undefined
      },
      getOrDefault: (k, d) => {
        if (k === 'dino:openapi:descriptor' || k === 'dino:openapi:descriptor:path') {
          return 'path/with/colon'
        }
        if (k === 'dino_openapi_descriptor') {
          return 'path/with/underscore'
        }
        if (k === 'DINO_OPENAPI_DESCRIPTOR') {
          return 'path/with/underscore/uppercase'
        }
      }
    } as EnvironmentConfiguration
    let testObj = new FilesystemDescriptorProvider(environment)
    // act
    let actual = testObj.provide()
    // assert
    assert.equal(actual, 'path/with/colon')
  })

  it('Load configuration from dino:openapi:descriptor variable', () => {
    let environment = {
      get: (k) => {
        if (k === 'dino:openapi:descriptor') {
          return 'path/with/underscore'
        }
      },
      getOrDefault: (k, _d) => {
        if (k === 'dino:openapi:descriptor:path') {
          return undefined
        }
      }
    } as EnvironmentConfiguration
    const testObj = new FilesystemDescriptorProvider(environment)
    // act
    let actual = testObj.provide()
    // assert
    assert.equal(actual, 'path/with/underscore')
  })

  it('Load configuration from dino:openapi:descriptor:path variable', () => {
    let environment = {
      get: (_k) => {
        return undefined
      },
      getOrDefault: (k, d) => {
        if (k === 'dino:openapi:descriptor:path') {
          return 'path/with/underscore'
        }
        if (k === 'dino:openapi:descriptor') {
          return undefined
        }
      }
    } as EnvironmentConfiguration
    let testObj = new FilesystemDescriptorProvider(environment)
    // act
    let actual = testObj.provide()
    // assert
    assert.equal(actual, 'path/with/underscore')
  })
})
