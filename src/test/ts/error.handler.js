//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

const { assert } = require('chai')
const { ErrorHandler } = require('../../main/ts/error.handler')

describe('Error Handler', () => {
  it('provide valid error handler', () => {
    let actual = ErrorHandler.instance({
      getOrDefault: (_key, def) => {
        return def
      }
    })
    assert.ok(actual)
  })

  it('error handler provide valid json', () => {
    let errorHandler = ErrorHandler.instance({
      getOrDefault: (_key, def) => {
        return def
      }
    })
    const res = {
      format: (o) => {
        return o.json()
      },
      status: (statusCode) => {
        assert.equal(statusCode, 500)
      },
      send: (actual) => {
        assert.equal(actual.message, 'test error')
      }
    }
    errorHandler(new Error('test error'), undefined, res, undefined)
  })
})
