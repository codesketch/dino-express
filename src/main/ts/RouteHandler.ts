//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { ApplicationContext, EnvironmentConfiguration, Logger, ObjectHelper } from 'dino-core'
import { RequestBodyParser } from './request/request.body.parser'
import { ErrorResponsePayload, Response, ResponsePayload } from './Response'
import { Helper } from './Helper'
import { NextFunction, Request } from 'express'
import { HttpException } from './exception/http.exception'
import { GenericObject, ResponseValidator } from './Types'
import { EventProducerInterface } from './events/EventProducerInterface'
import { ApplicationEvent } from './events/ApplicationEvent'
import { Interface } from './Interface'

// const DEFAULT_RESPONSE = { content: { 'application/json': { schema: { type: 'object', properties: { message: { type: 'string' } } } } } };

/**
 * Handles the route subject to the client request, it does act as a express middleware.
 *
 */
export class RouteHandler {
  private readonly api: any
  private serverType: string
  private readonly responseValidator: ResponseValidator
  private readonly applicationContext: ApplicationContext
  private readonly environment: EnvironmentConfiguration
  private readonly requestBodyParser: RequestBodyParser
  protected readonly eventProducer: EventProducerInterface

  private _interface: any

  constructor(
    applicationContext: ApplicationContext,
    api: any,
    responseValidator: ResponseValidator,
    environment: EnvironmentConfiguration,
    eventProducer: EventProducerInterface
  ) {
    this.api = api
    this.environment = environment
    this.responseValidator = responseValidator
    this.applicationContext = applicationContext
    this.requestBodyParser = new RequestBodyParser(environment)
    this.serverType = this.environment.getOrDefault('dino:server:type', 'express')
    this.eventProducer = eventProducer
  }

  /**
   * Handle the request from the client forwarding the request to the Interface that implement the
   * method defined on the API operation identifier.
   *
   * @param {any} req the express request object
   * @param {any} res the express response object
   * @param {Function} next a callback to propagate the handling action to the next middleware.
   *
   * @public
   */
  public handle(req: Request, res: Response, next: NextFunction): void {
    try {
      this.eventProducer.send(ApplicationEvent.create('workloadStarted'))
      this.ensureInterface()
      const answer = this._doHandleRequest(req)
      this.handleApiResponse(answer, res, next)
    } catch (e) {
      Logger.error(`Unable to complete transaction ${e}`)
      this.handleError(e as ErrorResponsePayload, res, next)
    }
  }

  _doHandleRequest(req: Request) {
    const apiHandlerType = Helper.getVariable(this.environment, 'dino:api:handler:type', 'proxy')
    let answer
    if ('proxy' == apiHandlerType) {
      const params = this.collectParameters(req)
      const body = this.requestBodyParser.parse(req)
      params['req'] = req
      params['body'] = body
      answer = this._interface[this.api.operationId](params)
    } else {
      // propagate the entire express request
      answer = this._interface[this.api.operationId](req)
    }
    return answer
  }

  /**
   * Collect the parameters defined as part of the OpenAPI definition.
   * @param {Object} req the express request
   */
  private collectParameters(req: Request) {
    const parameters = Helper.ensureValue(this.api.parameters, [])
    if (!parameters.reduce) {
      return []
    }
    return parameters.reduce((acc, param) => {
      const name = param.name.replace(/-/g, '_')
      switch (param.in) {
        case 'path':
          acc[name] = Helper.ensureValue(req.params[param.name], param.schema?.default)
          break
        case 'query':
          acc[name] = Helper.ensureValue(req.query[param.name], param.schema?.default)
          break
        case 'header':
          acc[name] = Helper.ensureValue(req.headers[param.name], param.schema?.default)
          break
      }
      return acc
    }, {})
  }

  handleApiResponse(answer: ResponsePayload | Promise<ResponsePayload>, res: Response, next: NextFunction) {
    if (answer === undefined) {
      return this.sendResponse(answer, res, next)
    }
    if (Helper.isAPromise(answer)) {
      return (answer as Promise<ResponsePayload>)
        .then((response: ResponsePayload) => this.sendResponse(response, res, next))
        .catch((error: Error) => this.handleError(error, res, next))
    } else {
      return this.sendResponse(answer, res, next)
    }
  }

  private handleError(error: ErrorResponsePayload, _response: Response, next: NextFunction) {
    this.eventProducer.send(ApplicationEvent.create('workloadError', { errorName: error?.name, errorMessage: error?.message }))
    let err: ErrorResponsePayload
    if (!ObjectHelper.instanceOf(error, HttpException)) {
      err = HttpException.create(500, error?.message ?? 'Unknown error')
    }
    next(err)
  }

  private sendResponse(answer: ResponsePayload, response: Response, next: NextFunction): void {
    this.eventProducer.send(ApplicationEvent.create('workloadCompleted'))
    const statusCode: string = Object.keys(this.api.responses || {}).find((sc) => sc.startsWith('2')) || '200'
    response.status(parseInt(statusCode, 10))
    response.setHeader('x-server-type', this.serverType)

    if (answer === undefined) {
      return next()
    }

    this.normaliseResponse(answer, response)

    if (this.responseValidator) {
      try {
        this.responseValidator({
          body: response.body,
          headers: response.getHeaders(),
          statusCode: response.statusCode
        })
      } catch (e) {
        return this.handleError(e as ErrorResponsePayload, response, next)
      }
    }
    next()
  }

  private normaliseResponse(answer: string | GenericObject, response: Response) {
    let setAnswerAsBody: boolean = true
    if (typeof answer == 'string') {
      response.body = answer
    } else {
      const temp: GenericObject = answer as GenericObject
      if (ObjectHelper.isDefined(temp.body)) {
        response.body = temp.body
        setAnswerAsBody = false
      }
      if (ObjectHelper.isDefined(temp.headers)) {
        Object.keys(temp.headers ?? {}).forEach((key: string) => {
          response.setHeader(key, temp.headers[key])
        })
      }
      if (setAnswerAsBody) {
        response.body = answer
      }
    }
  }

  /**
   * Lookup the interface that can handle the defined operationId for the API.
   * @returns {Interface}
   *
   * @private
   */
  private ensureInterface(): void {
    if (!this._interface) {
      const interfaces = this.applicationContext.resolveAll(Interface)
      this._interface = (interfaces || []).find((_interface) => {
        try {
          return Reflect.has(_interface, this.api.operationId)
        } catch (e) {
          return false
        }
      })
      if (!this._interface) {
        throw Error(`there are no interface defined with operationId ${this.api.operationId}`)
      }
    }
  }

  /**
   * Define tge scope as transient so that a new instance will be created for every request
   *
   * @static
   * @public
   */
  static scope() {
    return 'transient'
  }
}
