// Copyright 2025 Quirino Brizi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

import { ObjectHelper } from 'dino-core'
import { type NextFunction, type Request } from 'express'
import { ClientHttpException } from '../exception/client.http.exception'
import { type SecurityDefinition } from '../middlewares/impl/before/SecurityMiddleware'
import { type Response } from '../Response'
import { AbstractSecurityValidationHandler, type AuthType } from './AbstractSecurityValidationHandler'

// limitations under the License.
export abstract class HttpSecurityValidationHandler extends AbstractSecurityValidationHandler {
  public handle(req: Request, _res: Response, next: NextFunction, config: SecurityDefinition): void {
    const scheme: string | undefined = 'scheme' in config ? config.scheme : 'basic'
    const token: string | undefined = this.getToken(scheme, config, req)
    if (ObjectHelper.isDefined(token)) {
      let authorised: boolean = false
      if (scheme?.toLowerCase() === 'bearer') {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        authorised = this.validateBearerAuth(token!, config)
      }
      if (scheme?.toLowerCase() === 'basic') {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        authorised = this.validateBasicAuth(token!, config)
      }
      if (authorised) {
        next()
        return
      }
    }
    next(ClientHttpException.create(401, 'Unauthorized'))
  }

  public abstract validateBasicAuth(token: string, config: SecurityDefinition): boolean
  public abstract validateBearerAuth(token: string, config: SecurityDefinition): boolean

  public canHandle(type: AuthType): boolean {
    return type === 'http'
  }
}
