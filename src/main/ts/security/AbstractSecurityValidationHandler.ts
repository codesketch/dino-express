// Copyright 2025 Quirino Brizi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import type { NextFunction, Request } from 'express'
import { type OpenAPIV2, type OpenAPIV3 } from 'openapi-types'
import type { Response } from '../Response'
import { ClientHttpException } from '../exception/client.http.exception'
import { type SecurityDefinition } from '../middlewares/impl/before/SecurityMiddleware'

export type AuthType = 'http' | 'apiKey' | 'oauth2' | 'openIdConnect'

export abstract class AbstractSecurityValidationHandler {
  public abstract handle(req: Request, _res: Response, next: NextFunction, config: SecurityDefinition): void

  public abstract canHandle(type: AuthType): boolean

  protected getToken(scheme: string, config: SecurityDefinition, req: Request): string | undefined {
    if (['bearer', 'basic'].includes(scheme.toLowerCase())) {
      return req.headers.authorization
    }
    if (scheme.toLowerCase() === 'apiKey') {
      const c: OpenAPIV2.SecuritySchemeApiKey | OpenAPIV3.ApiKeySecurityScheme = config as
        | OpenAPIV2.SecuritySchemeApiKey
        | OpenAPIV3.ApiKeySecurityScheme
      return req[c.in][c.name]
    }
    throw ClientHttpException.create(401, 'not authorized')
  }
}
