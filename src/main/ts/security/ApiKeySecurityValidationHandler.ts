// Copyright 2025 Quirino Brizi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { ObjectHelper } from 'dino-core'
import { type NextFunction, type Request } from 'express'
import { ClientHttpException } from '../exception/client.http.exception'
import { type SecurityDefinition } from '../middlewares/impl/before/SecurityMiddleware'
import { type Response } from '../Response'
import { AbstractSecurityValidationHandler, type AuthType } from './AbstractSecurityValidationHandler'

export abstract class ApiKeySecurityValidationHandler extends AbstractSecurityValidationHandler {
  public handle(req: Request, _res: Response, next: NextFunction, config: SecurityDefinition): void {
    const token: string | undefined = this.getToken(this.getAuthType(), config, req)
    if (ObjectHelper.isDefined(token)) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      if (this.validateApiKeyAuth(token!, config)) {
        next()
      } else {
        next(ClientHttpException.create(401, 'Unauthorized'))
      }
    }
    next(ClientHttpException.create(401, 'Unauthorized'))
  }

  public abstract validateApiKeyAuth(token: string, config: SecurityDefinition): boolean

  public canHandle(type: AuthType): boolean {
    return type === this.getAuthType()
  }

  protected getAuthType(): AuthType {
    return 'apiKey'
  }
}
