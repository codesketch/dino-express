//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type Response as EResponse } from 'express'
import { type GenericCallback, type GenericObject } from './Types'
import { HttpException } from './exception/http.exception'
import { type CookieOptions, type Locals, type Application, type Request, type ParamsDictionary } from 'express-serve-static-core'
import { type ParsedQs } from 'qs'
import { type RuntimeContext } from './RuntimeContext'
import vary from 'vary'
import http from 'http'
import encodeUrl from 'encodeurl'
import { ResponseAdaptorStrategySelector } from './response/ResponseAdaptorStrategySelector'
import { mime } from 'send'
import { ObjectHelper } from 'dino-core'

export type ResponsePayload = GenericObject | string | undefined
export type ErrorResponsePayload = GenericObject | HttpException | Error | undefined

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
class EventEmitter {
  static captureRejectionSymbol: any
}

/**
 * The response that is returned from the API handler.
 * @public
 */
export interface Response extends EResponse {
  body: ResponsePayload
}

export class ResponseImpl extends http.ServerResponse implements Response {
  public body: ResponsePayload
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
  public app: Application<Record<string, any>> = {} as Application
  public locals: Record<string, any> & Locals = {}
  public charset: string = 'UTF-8'

  public readonly req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>
  private readonly callback: (...args: any) => void
  private readonly runtimeContext: RuntimeContext
  private readonly responseAdaptorStrategySelector: ResponseAdaptorStrategySelector = new ResponseAdaptorStrategySelector()

  public constructor(callback: GenericCallback<any>, req: Request, runtimeContext: RuntimeContext) {
    super(req)
    this.req = req
    this.callback = callback
    this.runtimeContext = runtimeContext
  }

  sendfile(_path: unknown, _options?: unknown, _fn?: unknown): void {
    throw new Error('Method not implemented.')
  }

  [EventEmitter.captureRejectionSymbol]?(_error: Error, _event: string, ..._args: any[]): void {
    throw new Error('Method not implemented.')
  }

  public status(code: number): any {
    this.statusCode = code
    return this
  }

  sendStatus(_code: number): this {
    throw new Error('Method not implemented.')
  }

  links(_links: any): this {
    throw new Error('Method not implemented.')
  }

  public send(_body?: GenericObject): any {
    this.callback(this.responseAdaptorStrategySelector.select(this.runtimeContext).adapt(this))
    return this
  }

  public json(body?: GenericObject): any {
    return this.send(body)
  }

  public jsonp(body?: GenericObject): any {
    return this.send(body)
  }

  public sendFile(_path: unknown, _options?: unknown, _fn?: unknown): void {
    throw new Error('Method not implemented.')
  }

  public download(_path: unknown, _filename?: unknown, _options?: unknown, _fn?: unknown): void {
    throw new Error('Method not implemented.')
  }

  public contentType(type: string): this {
    this.header('Content-Type', type)
    return this
  }

  type(type: string): this {
    const ct = !type.includes('/') ? mime.lookup(type) : type

    return this.set('Content-Type', ct)
  }

  public format(obj: GenericObject): this {
    const req = this.req
    const next = req.next
    const keys = Object.keys(obj).filter(function (v) {
      return v !== 'default'
    })
    const key: string | false | undefined = keys.length > 0 ? this.accepts(keys) : undefined
    this.vary('Accept')
    if (key !== undefined && typeof key === 'string') {
      this.set('Content-Type', ['json', 'default'].includes(key) ? 'application/json' : 'application/xml')
      obj[key](req, this, next)
    } else if (obj.default !== undefined) {
      obj.default(req, this, next)
    } else {
      if (next !== undefined) {
        next(HttpException.create(406, 'Not Acceptable'))
      }
    }
    return this
  }

  public accepts(types: string[]): string | undefined {
    const headers = this.req.headers
    if (ObjectHelper.isDefined(headers)) {
      const accept = headers.accept
      if (accept === 'application/json' && types.includes('json')) {
        return 'json'
      }
      if (accept === 'application/xml' && types.includes('xml')) {
        return 'xml'
      }
    }
    return 'json'
  }

  attachment(_filename?: string | undefined): this {
    throw new Error('Method not implemented.')
  }

  public set(field: string, value?: string | number | readonly string[]): this {
    if (value !== undefined) {
      this.setHeader(field, value)
    }
    return this
  }

  public header(field: string, value?: string | number | readonly string[]): this {
    this.set(field, value)
    return this
  }

  public get(field: string): string | undefined {
    return this.getHeader(field) as string
  }

  public clearCookie(_name: string, _options?: CookieOptions | undefined): this {
    throw new Error('Method not implemented.')
  }

  public cookie(_name: unknown, _val: unknown, _options?: unknown): this {
    throw new Error('Method not implemented.')
  }

  public location(url: string): this {
    let loc = url
    if (url === 'back') {
      loc = this.req.get('Referrer') ?? '/'
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    return this.set('Location', encodeUrl(loc))
  }

  public redirect(_url: unknown, _status?: unknown): void {
    throw new Error('Method not implemented.')
  }

  public render(_view: unknown, _options?: unknown, _callback?: unknown): void {
    throw new Error('Method not implemented.')
  }

  public vary(field: string): this {
    vary(this, field)
    return this
  }

  public append(field: string, value?: string | string[] | undefined): this {
    const prev = this.get(field)
    let val = value

    if (prev !== undefined) {
      val = Array.isArray(prev) ? prev.concat(value) : Array.isArray(value) ? [prev].concat(value) : [prev, value]
    }

    return this.set(field, val)
  }
}
