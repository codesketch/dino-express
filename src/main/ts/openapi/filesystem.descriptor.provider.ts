//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type EnvironmentConfiguration } from 'dino-core'

/**
 * File system OpenAPI descriptor provider.
 * @typedef FilesystemDescriptorProvider
 */
export class FilesystemDescriptorProvider {
  private readonly environment: EnvironmentConfiguration
  constructor(environment: EnvironmentConfiguration) {
    this.environment = environment
  }

  /**
   * Provide a reference to an OpenAPI descriptor stored on the local file-system, the location
   * of the descriptor can be provided using any of the `dino:openapi:descriptor`,
   * `dino_openapi_descriptor` or `DINO_OPENAPI_DESCRIPTOR` configuration variables,
   * if the configuration is not provided the default `src/main/resources/routes.yml` path will
   * be used.
   * @returns {string} the path of the OpenAPI descriptor file.
   */
  public provide(): string {
    const oldStyle = this.environment.get('dino:openapi:descriptor')
    const newStyle = this.environment.getOrDefault('dino:openapi:descriptor:path', 'src/main/resources/routes.yml')
    return newStyle ?? oldStyle
  }
}
