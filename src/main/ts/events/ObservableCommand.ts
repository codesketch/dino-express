import { type ApplicationContext, Command, type EnvironmentConfiguration } from 'dino-core'
import { type EventProducer } from './EventProducer'
import { ApplicationEvent } from './ApplicationEvent'
import { type GenericObject } from '../Types'

interface Deps {
  environment: EnvironmentConfiguration
  applicationContext: ApplicationContext
  eventProducer: EventProducer
}

/**
 * Extends Dino Command pattern definition adding observability, that's it before the command
 * is executed an event is generated with type `commandExecutionStarted`, when the command execution
 * is completed another event is generated with type `commandExecutionCompleted`, in case of error a
 * command is generated with type `commandExecutionFailed`.
 *
 * Custom logic is to be provided via the `doExecute` method.
 * The event payload can be customised using the `augmentEventPayload` method
 */
export abstract class ObservableCommand<I, R> extends Command<I, R> {
  protected readonly eventProducer: EventProducer
  constructor({ environment, applicationContext, eventProducer }: Deps) {
    super({ environment, applicationContext })
    this.eventProducer = eventProducer
  }

  async execute(input: I | undefined): Promise<R> {
    try {
      this.eventProducer.send(ApplicationEvent.create('commandExecutionStarted', this.augmentEventPayload('commandExecutionStarted', {})))
      return await this.doExecute(input)
    } catch (e) {
      this.eventProducer.send(ApplicationEvent.create('commandExecutionFailed', this.augmentEventPayload('commandExecutionFailed', {})))
      throw e
    } finally {
      this.eventProducer.send(ApplicationEvent.create('commandExecutionCompleted', this.augmentEventPayload('commandExecutionCompleted', {})))
    }
  }

  /**
   * Allows to provide custom logic that will be executed by this command
   * @returns the command execution response
   */
  protected abstract doExecute(_input: I | undefined): Promise<R>

  protected augmentEventPayload(
    _eventType: 'commandExecutionStarted' | 'commandExecutionCompleted' | 'commandExecutionFailed',
    payload: GenericObject
  ): GenericObject {
    return payload
  }

  /**
   * Enable lazy loading as the command will be used later in time, this prevent
   * loading during startup  improving performances.
   * @returns boolean flag indicating if this Injectable should be lazy loaded
   */
  lazy(): boolean {
    return true
  }
}
