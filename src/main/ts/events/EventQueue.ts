import { type ApplicationEvent } from './ApplicationEvent'
import { type EventEmitterInterface } from './EventEmitterInterface'
export class EventQueue {
  private readonly batchSize: number
  private readonly eventEmitter: EventEmitterInterface
  private readonly queue: ApplicationEvent[]

  private accept: boolean

  public constructor(eventEmitter: EventEmitterInterface, batchSize: number = 1) {
    this.queue = []
    this.accept = true
    this.batchSize = batchSize
    this.eventEmitter = eventEmitter
  }

  public add(applicationEvent: ApplicationEvent): boolean {
    if (!this.accept) {
      return false
    }
    const currentLength = this.queue.push(applicationEvent)
    if (currentLength >= this.batchSize) {
      const eventsToEmit = this.queue.splice(0, this.batchSize)
      void this.eventEmitter.emit(eventsToEmit)
    }
    return true
  }

  public async flush(): Promise<void> {
    this.accept = false
    await this.eventEmitter.emit(this.queue)
    this.queue.splice(0)
    this.accept = true
  }
}
