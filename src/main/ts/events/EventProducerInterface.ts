import { type ApplicationEvent } from './ApplicationEvent'

export interface EventProducerInterface {
  send: (applicationEvent: ApplicationEvent) => boolean
}
