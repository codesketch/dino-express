import { Logger } from 'dino-core'
import { type ApplicationEvent } from './ApplicationEvent'
import { type EventEmitterInterface } from './EventEmitterInterface'

/**
 * A simple event emitter which emits events as logs
 */
export class LogEventEmitter implements EventEmitterInterface {
  public async emit(events: ApplicationEvent[]): Promise<void> {
    events.forEach((event) => Logger.info(`Application Event -> ${JSON.stringify(event)}`))
  }
}
