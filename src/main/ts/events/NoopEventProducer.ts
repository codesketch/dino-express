import { Logger } from 'dino-core'
import { type ApplicationEvent } from './ApplicationEvent'
import { type EventProducerInterface } from './EventProducerInterface'

export class NoopEventProducer implements EventProducerInterface {
  public send(applicationEvent: ApplicationEvent): boolean {
    Logger.debug(`Application event: ${JSON.stringify(applicationEvent.asObject())}`)
    return false
  }
}
