import { type EnvironmentConfiguration } from 'dino-core'
import { type ApplicationEvent } from './ApplicationEvent'
import { type EventProducerInterface } from './EventProducerInterface'
import { type EventQueue } from './EventQueue'
import { type GenericObject } from '../Types'

export class EventProducer implements EventProducerInterface {
  private readonly eventQueue: EventQueue
  private readonly defaultInfo: GenericObject
  private readonly environment: EnvironmentConfiguration

  public constructor(eventQueue: EventQueue, environment: EnvironmentConfiguration, title: string, version: string) {
    this.eventQueue = eventQueue
    this.environment = environment
    this.defaultInfo = {
      application: {
        name: this.environment.getOrDefault('app:name', 'unknown'),
        title,
        version
      },
      environment: this.environment,
      serverType: this.environment.getOrDefault('dino:server:type', 'unknown'),
      cloud: this.environment.getOrDefault('dino:cloud', {})
    }
  }

  public send(applicationEvent: ApplicationEvent): boolean {
    this.enrich(applicationEvent)
    return this.eventQueue.add(applicationEvent)
  }

  protected enrich(applicationEvent: ApplicationEvent): void {
    applicationEvent.addToPayload(this.defaultInfo)
  }
}
