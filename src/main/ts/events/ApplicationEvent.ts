import { v4 } from 'uuid'
import { type GenericObject } from '../Types'
export class ApplicationEvent {
  private readonly id: string
  private readonly type: string
  private readonly timestamp: number
  private readonly payload: GenericObject

  protected constructor(id: string, type: string, payload: GenericObject) {
    this.id = id
    this.type = type
    this.timestamp = Date.now()
    this.payload = payload
  }

  public addToPayload(info: GenericObject): void {
    Object.assign(this.payload, info)
  }

  public asObject(): GenericObject {
    return {
      id: this.id,
      type: this.type,
      timestamp: this.timestamp,
      payload: this.payload
    }
  }

  public static create(type: string, payload: GenericObject = {}): ApplicationEvent {
    return new ApplicationEvent(v4(), type, payload)
  }
}
