import { type ApplicationEvent } from './ApplicationEvent'

export interface EventEmitterInterface {
  /**
   * Emits the events using the medium of the concrete class implementing this interface
   * @param events the events to propagate
   * @returns void
   */
  emit: (events: ApplicationEvent[]) => Promise<void>
}
