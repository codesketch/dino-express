//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type EnvironmentConfiguration } from 'dino-core'
import { type Request } from 'express'
import { get, set } from 'lodash'
import { Helper } from '../Helper'
import { type GenericObject } from '../Types'

/**
 * Parses the request body.
 *
 */
export class RequestBodyParser {
  private readonly environment: EnvironmentConfiguration
  /**
   * Create a new instance of RequestBodyParser
   * @param {EnvironmentConfiguration} environment the environment configuration
   */
  constructor(environment: EnvironmentConfiguration) {
    this.environment = environment
  }

  /**
   * Parse the body of the received request. Set forceBase64Decode to true will cause inspection
   * and deciding as  required of the top level properties of the received message, if the
   * received message contains the property isBase64Encoded this will take precedence above the
   * configuration settings.
   *
   * @param {Request} req the express request
   * @returns {object} the parsed body
   */
  public parse(req: Request | GenericObject): GenericObject | string | undefined {
    const forceBase64RequestDecode: boolean = Helper.getVariable(this.environment, 'dino:server:request:forceBase64Decode:enabled', false)
    const base64RequestDecodePaths: string[] = Helper.getVariable(this.environment, 'dino:server:request:forceBase64Decode:paths', [])
    let body = this.extractBody(req)
    if (body === undefined) {
      return
    }

    if (forceBase64RequestDecode) {
      body = this.parseAndDecodeFromBase64(body, base64RequestDecodePaths)
    }
    return body
  }

  /**
   * Customisable hook that allows to extract the body from the received request
   * @param {Request} req the received request
   * @returns {any} the request body
   * @protected
   */
  protected extractBody(req: Request | GenericObject): GenericObject | string {
    return Helper.requestHasBody(req) ? req.body : undefined
  }

  /**
   * Parse the request body decoding base64 string if present
   * @param {any} body the request body
   * @param {string[]} base64RequestDecodePaths an array of object pats that define the base64 strings to decode
   * @returns {any} the parsed and decoded request body
   * @private
   */
  private parseAndDecodeFromBase64(body: GenericObject | string, base64RequestDecodePaths: string[]): GenericObject | string {
    if (typeof body === 'string' && Helper.isBase64(body)) {
      body = Helper.decodeFromBase64AndParse(body)
    } else {
      if (base64RequestDecodePaths.length === 0) {
        // decoding paths have not been defined, attempt to decode
        // top level keys of the request
        body = Object.keys(body).reduce((decodedMessage: GenericObject, key: string) => {
          const element = body[key]
          if (typeof element === 'string') {
            // we inspect only the top level keys
            decodedMessage[key] = Helper.decodeFromBase64AndParse(element)
          } else {
            decodedMessage[key] = element
          }
          return decodedMessage
        }, {})
      } else {
        body = base64RequestDecodePaths.reduce<GenericObject>((decodedMessage: GenericObject, path: string) => {
          // we inspect only the top level keys
          const element: any = get(body, path)
          if (typeof element === 'string') {
            set(decodedMessage, path, Helper.decodeFromBase64AndParse(element))
          } else {
            set(decodedMessage, path, element)
          }
          return decodedMessage
          // eslint-disable-next-line @typescript-eslint/prefer-reduce-type-parameter
        }, body as GenericObject)
      }
    }
    return body
  }
}
