import { type EnvironmentConfiguration, ObjectHelper } from 'dino-core'
import { type GenericObject } from '../Types'
import { type Request } from 'express'
import { Helper } from '../Helper'
import * as querystring from 'querystring'
import { RequestBodyParser } from './request.body.parser'

export class AwsRequestAdaptor {
  private readonly exposes: string
  private readonly requestBodyParser: RequestBodyParser
  public constructor(environment: EnvironmentConfiguration) {
    this.requestBodyParser = new RequestBodyParser(environment)
    this.exposes = environment.getOrDefault('dino:server:expose', 'api')
  }

  public adapt(message: GenericObject): Request {
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    return {
      url: this.buildUrl(message),
      method: this.getMethod(message),
      path: Helper.ensureValue(message.path, '/'),
      body: this.requestBodyParser.parse(message),
      headers: this.getHeaders(message),
      query: this.getQuery(message)
    } as Request
  }

  /**
   * Build the request URL if required
   * @param {Object} message the request message
   * @returns {String}
   *
   * @private
   */
  private buildUrl(message: GenericObject): string {
    if (ObjectHelper.isDefined(message.url)) {
      return message.url
    }
    return `${this.getOriginalUrl(message)}`
  }

  private getHeaders(message: GenericObject): GenericObject {
    const headers = Helper.ensureValue(message.headers, {})
    if (ObjectHelper.isNotDefined(headers['transfer-encoding'])) {
      headers['transfer-encoding'] = 'identity'
    }
    if (ObjectHelper.isNotDefined(headers.accept)) {
      headers.accept = 'application/json'
    }
    return headers
  }

  private getOriginalUrl(message: GenericObject): string {
    if (message.originalUrl !== undefined) {
      return message.originalUrl
    }
    const originalUrl = `${Helper.ensureValue(message.resource, '/')}?${querystring.stringify(this.getQuery(message))}`
    if (!originalUrl.startsWith('/')) {
      return `/${originalUrl}`
    }
    return originalUrl
  }

  private getMethod(message: GenericObject): string {
    const _default = this.exposes.toLowerCase() === 'api' ? 'GET' : 'POST'
    return message.method ?? message.httpMethod ?? _default
  }

  private getQuery(message: GenericObject): GenericObject {
    return message.multiValueQueryStringParameters ?? message.query ?? message.querystring ?? {}
  }
}
