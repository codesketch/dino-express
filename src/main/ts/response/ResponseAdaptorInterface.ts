import { type Response } from '../Response'
import { type GenericObject } from '../Types'

export interface ResponseAdaptorInterface {
  adapt: (response: Response) => GenericObject
}
