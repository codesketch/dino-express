import { type Response } from '../../Response'
// Copyright 2022 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { type EnvironmentConfiguration, Logger } from 'dino-core'
import { Helper } from '../../Helper'
import { type GenericObject } from '../../Types'
import { type ResponseAdaptorInterface } from '../ResponseAdaptorInterface'

export class LambdaResponseAdaptor implements ResponseAdaptorInterface {
  private readonly responseAsBase64: boolean
  /**
   * Valid values are proxy and method, need to create an enum
   */
  private readonly integrationType: string

  public constructor(environment: EnvironmentConfiguration) {
    this.responseAsBase64 = environment.getOrDefault('dino:server:response:asBase64', false)
    // if there is a typo default to method integration type...
    this.integrationType = environment.getOrDefault('dino:cloud:integration', 'method')
  }

  public adapt(response: Response): GenericObject {
    const res = this.integrationType === 'proxy' ? this.buildAWSProxyResponse(response) : this.buildAWSResponse(response)
    Logger.debug(`sending response ${JSON.stringify(res)} built for ${this.integrationType} on AWS Lambda`)
    return res
  }

  private buildAWSResponse(response: Response): GenericObject {
    const headers = response.getHeaders() ?? {}
    headers['X-Cloud-Provider'] = 'AWS'
    return {
      statusCode: response.statusCode ?? 200,
      headers,
      body: Helper.buildResponseBody(response, false, this.responseAsBase64),
      isBase64Encoded: this.responseAsBase64
    }
  }

  private buildAWSProxyResponse(response: Response): GenericObject {
    const headers = response.getHeaders() ?? {}
    headers['X-Cloud-Provider'] = 'AWS'
    return {
      statusCode: response.statusCode ?? 200,
      headers,
      multiValueHeaders: {},
      body: Helper.buildResponseBody(response, true, this.responseAsBase64),
      isBase64Encoded: this.responseAsBase64
    }
  }
}
