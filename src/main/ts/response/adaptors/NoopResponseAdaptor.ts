import { type Response } from '../../Response'
import { type GenericObject } from '../../Types'
import { type ResponseAdaptorInterface } from '../ResponseAdaptorInterface'

export class NoopResponseAdaptor implements ResponseAdaptorInterface {
  public adapt(response: Response): GenericObject {
    const answer = response.body
    if (answer === undefined) {
      return {}
    }
    return answer as GenericObject
  }
}
