import { type RuntimeContext } from '../RuntimeContext'
import { LambdaResponseAdaptor } from './adaptors/LambdaResponseAdaptor'
import { NoopResponseAdaptor } from './adaptors/NoopResponseAdaptor'
import { type ResponseAdaptorInterface } from './ResponseAdaptorInterface'

export class ResponseAdaptorStrategySelector {
  public select(runtimeContext: RuntimeContext): ResponseAdaptorInterface {
    if (runtimeContext.isRunningOnAwsLambda()) {
      return new LambdaResponseAdaptor(runtimeContext.getEnvironment())
    }
    return new NoopResponseAdaptor()
  }
}
