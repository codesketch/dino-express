/* eslint-disable @typescript-eslint/strict-boolean-expressions */
/* eslint-disable @typescript-eslint/indent */
import { HttpException } from './exception/http.exception'
import { type Response } from './Response'
//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type EnvironmentConfiguration, ObjectHelper } from 'dino-core'
import { type NextFunction, type Request } from 'express'
import errorHandler from 'express-error-handler'
import { ValidationError } from 'express-openapi-validate'

/**
 * The error handler used to define the express error handling behaviour
 * @public
 */
// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class ErrorHandler {
  /**
   * Allows to get an instance of the error handler
   */
  public static instance(environment: EnvironmentConfiguration): any {
    const cloudProvider = environment.getOrDefault('dino:cloud:provider', 'google')
    if (cloudProvider === 'aws') {
      // AWS doesn't have a format method on response
      return (err: Error, _req: Request, res: Response, next: NextFunction) => {
        // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
        const statusCode = ObjectHelper.instanceOf(err, HttpException)
          ? (err as HttpException).getStatusCode()
          : ObjectHelper.instanceOf(err, ValidationError)
          ? (err as ValidationError).statusCode
          : 500
        const body = {
          status: statusCode,
          message: err.message
        }
        if (statusCode >= 400 && statusCode <= 499) {
          ;['code', 'name', 'type', 'details'].forEach(function (prop) {
            // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
            if (err[prop]) body[prop] = err[prop]
          })
        }
        res.body = body
        res.statusCode = statusCode
        next()
      }
    } else {
      return (err: Error, req: Request, res: Response, next: NextFunction) => {
        // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
        const statusCode = ObjectHelper.instanceOf(err, HttpException) ? (err as HttpException).getStatusCode() : 500
        const handler = errorHandler({
          shutdown: () => {},
          serializer: function (err) {
            const body = {
              status: statusCode,
              message: err.message
            }
            // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
            if (errorHandler.isClientError(err.status)) {
              ;['code', 'name', 'type', 'details'].forEach(function (prop) {
                // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
                if (err[prop]) body[prop] = err[prop]
              })
            }
            return body
          }
        })
        // eslint-disable-next-line @typescript-eslint/dot-notation
        err['status'] = statusCode
        return handler(err, req, res, next)
      }
    }
  }
}
