//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Configuration, ComponentDescriptor, Scope } from 'dino-core'
import { RoutingConfigurer } from './RoutingConfigurer'

/**
 * Define the configuration structures for the swagger based application routing
 *
 * Application should extend this configuration in order to autoconfigure the express routing.
 *
 * @public
 */
export class WebConfiguration extends Configuration {
  public configure(): ComponentDescriptor | Promise<ComponentDescriptor> {
    return ComponentDescriptor.createFromType('routingConfigurer', RoutingConfigurer, Scope.SINGLETON)
  }
}
