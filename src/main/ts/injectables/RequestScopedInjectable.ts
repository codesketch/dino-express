import { Injectable } from 'dino-core'
import expressHttpContext from 'express-http-context2'
import { type HttpContext } from '../Types'

export class RequestScopedInjectable extends Injectable {
  /**
   * Allows to access the current http context
   * @returns the http context
   */
  protected getHttpContext(): HttpContext {
    const httpContext = expressHttpContext.get('httpContext')
    if (httpContext === undefined) {
      return {
        headers: {},
        query: {}
      }
    }
    return httpContext
  }
}
