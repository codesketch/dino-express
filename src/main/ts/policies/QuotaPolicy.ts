//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type Request } from 'express'
import { type Response } from '../Response'
import { AbstractPolicy } from './AbstractPolicy'
import { Logger } from 'dino-core'

interface BucketRecord {
  count: number
  expires: number
}

interface Configuration {
  resetInterval: number
  window: number
  allow: number
}

export class QuotaPolicy extends AbstractPolicy {
  private readonly buckets: Map<string, BucketRecord>
  private configuration!: Configuration
  private cleanUpJob!: NodeJS.Timeout

  public constructor() {
    super()
    this.buckets = new Map<string, BucketRecord>()
  }

  public preDestroy(): void {
    clearInterval(this.cleanUpJob)
  }

  /**
   * Apply the quota policy. The quota is calculated based on **window** and **allow** configuration
   * parameters and based on the IP address of the remote client.
   *
   * @param {Any} req the client request
   * @returns true if the number of requests on the defined window is less than the allow configuration
   * parameter for the specific client, false otherwise
   *
   * @protected
   * @implements AbstractPolicy#apply(req)
   */

  public apply(req: Request): boolean {
    const now = Date.now()
    const key = (req.headers['x-forwarded-for'] ?? req.socket.remoteAddress) as string
    let bucket = this.buckets.get(key)
    if (bucket === undefined) {
      bucket = { count: 0, expires: now + this.configuration.window }
      this.buckets.set(key, bucket)
    }

    if (now > bucket.expires) {
      bucket.count = 0
      bucket.expires = now + this.configuration.window
    }
    bucket.count += 1
    Logger.debug('Bucket', bucket)
    return bucket.count <= this.configuration.allow
  }

  public configure(configuration: Configuration): void {
    this.configuration = configuration
    this.cleanUpJob = setInterval(this.removeExpired.bind(this), configuration.resetInterval ?? 30000)
  }

  protected onDeny(res: Response): void {
    res.status(429)
    res.body = { message: 'Too Many Requests' }
  }

  /**
   * Remove expired buckets
   *
   * @private
   */
  private removeExpired(): void {
    Logger.debug('remove expires buckets')
    const now = Date.now()
    for (const key of this.buckets.keys()) {
      const bucket = this.buckets.get(key)
      if (bucket !== undefined && now > bucket.expires) {
        this.buckets.delete(key)
      }
    }
  }
}
