//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type AbstractPolicy } from './AbstractPolicy'
import { QuotaPolicy } from './QuotaPolicy'
import { RateLimitPolicy } from './RateLimitPolicy'

export class PolicyFactory {
  /**
   * Get a policy object
   * @param {String} policy the policy to lookup
   * @returns {AbstractPolicy} an implementation of AbstractPolicy
   *
   * @public
   */
  public getPolicy(policy: string): AbstractPolicy {
    if (['quotaPolicy', 'quota'].includes(policy)) {
      return new QuotaPolicy()
    } else if (['rateLimit', 'rateLimitPolicy', 'rate'].includes(policy)) {
      return new RateLimitPolicy()
    }
    throw new Error(`unknown policy ${policy}, available are: quotaPolicy and rateLimitPolicy`)
  }
}
