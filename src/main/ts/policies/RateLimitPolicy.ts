//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type Request } from 'express'
import { type Response } from '../Response'
import { AbstractPolicy } from './AbstractPolicy'
import { Logger } from 'dino-core'

interface BucketRecord {
  used: number
  windowExpires: number
}

interface Configuration {
  interval: number
  allow: number
}

/**
 * Allows to control the number of requests received by the API in the time
 * interval
 */
export class RateLimitPolicy extends AbstractPolicy {
  private configuration!: Configuration
  private readonly buckets: Map<string, BucketRecord>

  public constructor() {
    super()
    this.buckets = new Map<string, BucketRecord>()
  }

  public apply(req: Request): boolean {
    const now = Date.now()
    const key = (req.headers['x-forwarded-for'] ?? req.socket.remoteAddress) as string
    const weight = 1
    const windowSize = this.configuration.interval / 1000 / this.configuration.allow

    let allowed = true
    let bucket = this.buckets.get(key)
    if (bucket === undefined) {
      bucket = { used: 0, windowExpires: 0 }
      this.buckets.set(key, bucket)
    }

    Logger.debug('Bucket', bucket)

    if (now < bucket.windowExpires) {
      bucket.used = bucket.used + weight
      allowed = false
    } else {
      this.buckets.set(key, bucket)
      bucket.windowExpires = now + weight * windowSize
      bucket.used = weight
    }

    return allowed
  }

  public configure(configuration: Configuration): void {
    this.configuration = configuration
  }

  protected onDeny(res: Response): void {
    res.status(429)
    res.body = { message: 'Too Many Requests' }
  }
}
