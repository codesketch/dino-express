//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type Response } from '../Response'
import { type NextFunction, type Request } from 'express'

export abstract class AbstractPolicy {
  /**
   * Execute the policy logic and allow integration with express
   * @param req the client request
   * @param res the client response
   * @param next a callback invoked when the middleware terminates successfully
   *
   * @public
   */
  public middleware(req: Request, res: Response, next: NextFunction): void {
    this.apply(req) ? next() : this.onDeny(res)
  }

  /**
   * Apply the requested policy
   * @param {Any} req the incoming request
   * @returns {Boolean} true if the policy once applied allow access to the API, false otherwise
   *
   * @protected
   * @abstract
   */
  public abstract apply(_req: Request): boolean

  /**
   * Allow to configure this policy
   * @param {Any} configuration the policy configuration
   *
   * @public
   * @abstract
   */
  public abstract configure(_configuration: any): void

  /**
   * Execute logic that will signal that the policy has denied the client request
   * @param {Any} res the client response
   *
   * @abstract
   * @protected
   */
  protected abstract onDeny(_res: Response): void
}
