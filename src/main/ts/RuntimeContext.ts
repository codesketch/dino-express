import { type EnvironmentConfiguration } from 'dino-core'
import { type CloudProvider, type ServerType } from './Types'

/**
 * Provide the runtime context where for the execution
 */
export class RuntimeContext {
  private readonly serverType: ServerType
  private readonly cloudProvider: CloudProvider
  private readonly cloudService: string
  private readonly environment: EnvironmentConfiguration

  public constructor(environment: EnvironmentConfiguration) {
    this.environment = environment
    this.serverType = this.environment.getOrDefault('dino:server:type', 'express')
    this.cloudProvider = this.environment.getOrDefault('dino:cloud:provider', 'google')
    this.cloudService = this.environment.getOrDefault('dino:cloud:service', 'function')
  }

  public getEnvironment(): EnvironmentConfiguration {
    return this.environment
  }

  public isStandalone(): boolean {
    return ['express', 'standalone'].includes(this.serverType)
  }

  /**
   * Validate if the configuration identifies an embedded runtime, a runtime is embedded
   * if the server is provided from an external sources for instance an AWS lambda or GCP
   * function
   *
   * @returns true if the configuration identifies an embedded runtime, false otherwise
   */
  public isEmbedded(): boolean {
    return ['serverless', 'embedded'].includes(this.serverType)
  }

  public isRunningOnAwsLambda(): boolean {
    // TODO: QB serverType check is here for backward compatibility and will be removed soon
    return this.cloudProvider === 'aws' && (this.cloudService === 'lambda' || this.isEmbedded())
  }

  public isMonitoringEnabled(): boolean {
    return this.environment.getOrDefault('dino:server:monitoring:enabled', true)
  }

  public isCo2MonitoringEnabled(): boolean {
    return this.environment.getOrDefault('dino:server:monitoring:co2', true)
  }
}
