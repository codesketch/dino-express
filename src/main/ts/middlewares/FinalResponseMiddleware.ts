import finalhandler from 'finalhandler'
import { type Response } from '../Response'
import { type Request } from 'express'
import { type EnvironmentConfiguration, Logger, ObjectHelper } from 'dino-core'
export class FinalResponseMiddleware {
  private readonly environment: EnvironmentConfiguration

  public constructor(environment: EnvironmentConfiguration) {
    this.environment = environment
  }

  handle(req: Request, res: Response, _callback: (body: any) => void, reject: (reason?: any) => void): (err?: any) => void {
    const cloudProvider = this.environment.getOrDefault('dino:cloud:provider', 'google')
    Logger.info(`handling response for ${cloudProvider} cloud provider`)
    if (cloudProvider === 'aws') {
      return (err: Error) => {
        if (ObjectHelper.isDefined(err)) {
          reject(err)
        }
      }
    } else {
      return finalhandler(req, res, {
        env: this.environment.getOrDefault('dino:environment', process.env.NODE_ENV),
        onerror: Logger.error.bind(this)
      })
    }
  }
}
