import expressHttpContext from 'express-http-context2'
import { type Request, type NextFunction } from 'express'
import { type Response } from '../../../Response'
import { BeforeHandlerMiddleware } from '../../before.handler.middleware'
import { type HttpContext } from '../../../Types'

export class ScopeCreatorMiddleware extends BeforeHandlerMiddleware {
  public handle(req: Request, _res: Response, next: NextFunction): void {
    expressHttpContext.set('httpContext', {
      headers: req.headers,
      query: req.query
    } satisfies HttpContext)
    next()
  }
}
