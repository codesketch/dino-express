import { type ApplicationContext, ObjectHelper } from 'dino-core'
import { type NextFunction, type Request } from 'express'
import { type OpenAPI, type OpenAPIV2, type OpenAPIV3, type OpenAPIV3_1 } from 'openapi-types'
import { type Response } from '../../../Response'
import { AbstractSecurityValidationHandler, type AuthType } from '../../../security/AbstractSecurityValidationHandler'
import { BeforeHandlerMiddleware } from '../../before.handler.middleware'

// interface SecurityConfiguration {
//   enabled: boolean
//   authenticator: string
// }
export type SecurityDefinition = OpenAPIV2.SecuritySchemeObject | OpenAPIV3_1.ReferenceObject | OpenAPIV3.SecuritySchemeObject
type SecurityDefinitions = Record<string, OpenAPIV2.SecuritySchemeObject | OpenAPIV3_1.ReferenceObject | OpenAPIV3.SecuritySchemeObject> | undefined

export class SecurityMiddleware extends BeforeHandlerMiddleware {
  private readonly api: OpenAPI.Operation
  private readonly apiDocument: OpenAPI.Document
  private readonly applicationContext: ApplicationContext

  public constructor(applicationContext: ApplicationContext, api: OpenAPI.Operation, components: OpenAPI.Document) {
    super()
    this.api = api
    this.apiDocument = components
    this.applicationContext = applicationContext
  }

  /**
   * Definition of the function that will handle the security business logic associated with this middleware.
   *
   * @param {import('express').Request} req Express request instance
   * @param {import('express').Response} res express response instance
   * @param {Function} next the next function that will be invoked as part of the express chain
   */
  handle(req: Request, res: Response, next: NextFunction): void {
    const securityDefinitions: OpenAPIV2.SecurityRequirementObject[] | OpenAPIV3.SecurityRequirementObject[] | undefined = this.api.security
    if (ObjectHelper.isDefined(securityDefinitions)) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      for (let index = 0; index < securityDefinitions!.length; index++) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const securityRequirement: OpenAPIV2.SecurityRequirementObject | OpenAPIV3.SecurityRequirementObject = securityDefinitions![index]
        for (const securityDefinitionName of Object.keys(securityRequirement)) {
          const securityDefinitions: SecurityDefinitions = this.getSecurityDefinition(this.apiDocument)
          if (ObjectHelper.isDefined(securityDefinitions)) {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            const config: SecurityDefinition = securityDefinitions![securityDefinitionName]
            if ('type' in config) {
              // if type is not present means it is a ref, that case needs to be implemented
              const handlers: AbstractSecurityValidationHandler[] = this.applicationContext.resolveAll(AbstractSecurityValidationHandler)
              const handler: AbstractSecurityValidationHandler | undefined = handlers.find((handler: AbstractSecurityValidationHandler) =>
                handler.canHandle(config.type as AuthType)
              )
              if (ObjectHelper.isDefined(handler)) {
                return handler?.handle(req, res, next, config)
              }
            }
          }
        }
      }
    }
    next()
  }

  protected getSecurityDefinition(document: OpenAPI.Document): SecurityDefinitions {
    if ('securityDefinitions' in document) {
      return document.securityDefinitions
    }
    if ('components' in document) {
      return document.components?.securitySchemes
    }
    return undefined
  }
}
