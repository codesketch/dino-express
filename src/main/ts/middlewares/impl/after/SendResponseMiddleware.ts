import { type Request, type NextFunction } from 'express'
import { type Response } from '../../../Response'
import { AfterHandlerMiddleware } from '../../after.handler.middleware'
import { Helper } from '../../../Helper'

export class SendResponseMiddleware extends AfterHandlerMiddleware {
  public handle(_req: Request, res: Response, _next: NextFunction): void {
    res.format({
      xml: () => {
        res.send(Helper.format(res.body, 'application/xml'))
      },
      json: () => {
        res.send(res.body)
      },
      default: () => {
        res.send(res.body)
      }
    })
  }
}
