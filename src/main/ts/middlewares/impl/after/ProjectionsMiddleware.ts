import { Response } from '../../../Response'
import { ObjectHelper } from 'dino-core'
import { NextFunction, Request } from 'express'
import { AfterHandlerMiddleware } from '../../after.handler.middleware'
import { PropertyName, pick } from 'lodash'

export class ProjectionsMiddleware extends AfterHandlerMiddleware {
  /**
   * Definition of the function that will handle the projections business logic associated with this middleware.
   *
   * @param {import('express').Request} req Express request instance
   * @param {import('express').Response} res express response instance
   * @param {Function} next the next function that will be invoked as part of the express chain
   */
  public handle(req: Request, res: Response, next: NextFunction): void {
    const projections: string | string[] | undefined = req.headers['x-project']
    if (ObjectHelper.isNotDefined(projections)) {
      return next()
    }
    const filters: PropertyName[] = (typeof projections === 'string' ? projections.split(',') : projections) as PropertyName[]
    if (Array.isArray(res.body)) {
      res.body = res.body.map((obj) => pick(obj, filters))
    } else {
      res.body = pick(res.body as any, filters)
    }
    next()
  }
}
