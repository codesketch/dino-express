// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { type NextFunction, type Request } from 'express'
import { type Response } from '../Response'
/**
 * Define the interface for a middleware that will be executed before the request handler
 *
 * @typedef BeforeHandlerMiddleware
 * @public
 */
export abstract class BeforeHandlerMiddleware {
  /**
   * Abstract definition of the function that will handle the business logic associated with this middleware.
   *
   * @param {import('express').Request} req Express request instance
   * @param {import('express').Response} res express response instance
   * @param {Function} next the next function that will be invoked as part of the express chain
   */
  public abstract handle(_req: Request, _res: Response, _next: NextFunction): void
}
