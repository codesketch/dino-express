import { ErrorHandler, type ErrorType, Logger } from 'dino-core'
import { ApplicationEvent } from './events/ApplicationEvent'
import { type EventProducerInterface } from './events/EventProducerInterface'
export class GlobalErrorHandler extends ErrorHandler {
  private readonly eventProducer: EventProducerInterface

  constructor(eventProducer: EventProducerInterface) {
    super()
    this.eventProducer = eventProducer
  }

  handle(error: Error, type: string | ErrorType): void {
    Logger.error(`${type}, ${error.name} - ${error.message}`)
    this.eventProducer.send(ApplicationEvent.create('workloadError', { errorName: error.name, errorMessage: error.message }))
  }
}
