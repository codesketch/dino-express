import { type Request, type NextFunction, type Router } from 'express'
import { type AfterHandlerMiddleware } from './middlewares/after.handler.middleware'
import { type BeforeHandlerMiddleware } from './middlewares/before.handler.middleware'
import { type Response } from './Response'

export type ServerType = 'express' | 'serverless' | 'standalone' | 'embedded'
export type CloudProvider = 'aws' | 'google'
export type CloudService = 'lambda' | 'function'

export type MiddlewareHandler = (req: Request, res: Response, next: NextFunction) => void
export type MiddlewareErrorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => void
export type Middlewares = Array<MiddlewareHandler | MiddlewareErrorHandler | AfterHandlerMiddleware | BeforeHandlerMiddleware>
export type GenericCallback<T> = (...args: T[]) => void
export interface OrderedMiddlewares {
  before: Middlewares
  after: Middlewares
}

export type GenericObject = Record<string, any>
export type ResponseValidator = (res: GenericObject) => void
export interface RouterWithHandler extends Router {
  handle: (req: Request, res: Response, next: NextFunction) => void
}

export type PathsObject = Record<string, GenericObject | undefined>

export interface Policy {
  name: string
  configuration: any
}

export interface HttpContext {
  headers: GenericObject
  query: GenericObject
}
