import { ResponseImpl, type Response } from './Response'
import { type ApplicationContext, Dino, type EnvironmentConfiguration, Logger, ObjectHelper } from 'dino-core'
import { type RouterWithHandler } from './Types'
import { type Request } from 'express'
import { FinalResponseMiddleware } from './middlewares/FinalResponseMiddleware'
import { AwsRequestAdaptor } from './request/AwsRequestAdaptor'
import { type RuntimeContext } from './RuntimeContext'

export class Server {
  private runtimeContext!: RuntimeContext
  private environment!: EnvironmentConfiguration
  private applicationContext!: ApplicationContext
  private intervalTimer!: NodeJS.Timeout
  private router!: RouterWithHandler
  private finalResponseHandler!: FinalResponseMiddleware
  private awsRequestAdaptor!: AwsRequestAdaptor

  public async load(config?: string): Promise<(req: Request, res: Response) => Promise<any>> {
    this.applicationContext = await Dino.run(config)
    this.environment = this.applicationContext.resolve('environment')
    this.awsRequestAdaptor = new AwsRequestAdaptor(this.environment)
    this.finalResponseHandler = new FinalResponseMiddleware(this.environment)
    this.runtimeContext = this.applicationContext.resolve('dinoRuntimeContext')
    if (this.runtimeContext.isEmbedded()) {
      this.intervalTimer = setInterval(this.discoverRouter.bind(this), 10)
    }
    return this.run.bind(this)
  }

  public async run(req: Request, res: Response): Promise<any> {
    if (this.runtimeContext.isStandalone()) {
      // the context will be loaded and the express server will be started
      await Promise.resolve()
    } else {
      return await this.handleServerlessMode(req, res)
    }
  }

  private async handleServerlessMode(req: Request, res: Response): Promise<any> {
    return await new Promise((resolve, reject): void => {
      let request: Request
      let response: Response
      if (this.runtimeContext.isRunningOnAwsLambda()) {
        // adapt aws request to express request
        request = this.awsRequestAdaptor.adapt(req)
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        response = new ResponseImpl(resolve, request, this.runtimeContext)
      } else {
        request = req
        response = res
      }
      if (ObjectHelper.isDefined(this.router)) {
        Logger.debug('router has already been initialized...')
        this.router.handle(request, response, this.finalResponseHandler.handle(request, response, resolve, reject))
      } else {
        Logger.info('router not found initialize it...')
        const resolver = (): any => {
          this.router = this.applicationContext.resolve('dinoApiRouter')
          clearInterval(this.intervalTimer)
          this.router.handle(request, response, this.finalResponseHandler.handle(request, response, resolve, reject))
        }
        setTimeout(resolver.bind(this), 500)
      }
    })
  }

  private discoverRouter(): void {
    try {
      this.router = this.applicationContext.resolve('dinoApiRouter')
      if (ObjectHelper.isDefined(this.router)) {
        clearInterval(this.intervalTimer)
      }
    } catch (error) {
      // nothing to do
    }
  }
}
