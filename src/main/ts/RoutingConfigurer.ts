//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import SwaggerParser from '@apidevtools/swagger-parser'
import apicache from 'apicache-plus'
import cors from 'cors'
import { ApplicationContext, Component, ComponentDescriptor, EnvironmentConfiguration, Logger, Monitor, ObjectHelper, Scope } from 'dino-core'
import express, { Application, NextFunction, Request, RequestHandler, Router } from 'express'
import expressHttpContext from 'express-http-context2'
import { InfoObject } from 'express-openapi-validate/dist/OpenApiDocument'
import https from 'https'
import { OpenAPI } from 'openapi-types'
import { serve, setup } from 'swagger-ui-express'
import { GlobalErrorHandler } from './GlobalErrorHandler'
import { Helper } from './Helper'
import { Response } from './Response'
import { RouteHandler } from './RouteHandler'
import { RuntimeContext } from './RuntimeContext'
import { MiddlewareErrorHandler, MiddlewareHandler, Middlewares, OrderedMiddlewares, Policy, ResponseValidator } from './Types'
import { ApiValidator } from './api.validator'
import { ErrorHandler } from './error.handler'
import { ApplicationEvent } from './events/ApplicationEvent'
import { EventEmitterInterface } from './events/EventEmitterInterface'
import { EventProducer } from './events/EventProducer'
import { EventProducerInterface } from './events/EventProducerInterface'
import { EventQueue } from './events/EventQueue'
import { LogEventEmitter } from './events/LogEventEmitter'
import { NoopEventProducer } from './events/NoopEventProducer'
import { AfterHandlerMiddleware } from './middlewares/after.handler.middleware'
import { BeforeHandlerMiddleware } from './middlewares/before.handler.middleware'
import { ProjectionsMiddleware } from './middlewares/impl/after/ProjectionsMiddleware'
import { SendResponseMiddleware } from './middlewares/impl/after/SendResponseMiddleware'
import { ScopeCreatorMiddleware } from './middlewares/impl/before/ScopeCreatorMiddleware'
import { SecurityMiddleware } from './middlewares/impl/before/SecurityMiddleware'
import { ApplicationStateMonitor } from './monitoring/application.state.monitor'
import { CachePerformanceMonitor } from './monitoring/cache.performance.monitor'
import { CO2EmissionMonitor } from './monitoring/co2.emission.monitor'
import { DescriptorProvider } from './openapi/descriptor.provider'
import { FilesystemDescriptorProvider } from './openapi/filesystem.descriptor.provider'
import { PolicyFactory } from './policies/PolicyFactory'

const DESCRIPTOR_PROVIDER_MAP = Object.freeze({ filesystem: FilesystemDescriptorProvider })

type Deps = { applicationContext: ApplicationContext; environment: EnvironmentConfiguration }

/**
 * Configure express routing based on swagger configuration.
 *
 * @typedef RoutingConfigurer
 * @public
 */
export class RoutingConfigurer extends Component {
  private server: any
  private applicationStateMonitor!: ApplicationStateMonitor
  protected readonly environment: EnvironmentConfiguration
  protected readonly applicationContext: ApplicationContext
  protected readonly runtimeContext: RuntimeContext
  protected readonly policyFactory: PolicyFactory

  protected eventProducer!: EventProducerInterface

  constructor({ applicationContext, environment }: Deps) {
    super()

    this.server = undefined

    this.environment = environment
    this.applicationContext = applicationContext
    this.runtimeContext = new RuntimeContext(environment)
    applicationContext.register('dinoRuntimeContext', ComponentDescriptor.createFromValue('dinoRuntimeContext', this.runtimeContext, Scope.SINGLETON))
    this.policyFactory = new PolicyFactory()
  }

  public async postConstruct() {
    const app: Application = express()
    const payloadSize = this.environment.getOrDefault('dino:server:payloadSize', '100kb')
    app.use(express.json({ limit: payloadSize }))
    app.disable('x-powered-by')
    app.use(expressHttpContext.middleware)
    this.extend(app)
    const router: Router = express.Router()
    const parser = new SwaggerParser()
    const apis: OpenAPI.Document = await parser.dereference(this.resolveDescriptorProvider().provide())
    const apiValidator = new ApiValidator(this.environment)
    apiValidator.init(apis)
    this.eventProducer = this.configureApplicationObservability(apis.info)
    this.applicationContext.add(ComponentDescriptor.createFromValue('errorHandler', this.getGlobalErrorHandler(), Scope.SINGLETON))

    if (apis.paths !== undefined) {
      const docsPath = this.environment.getOrDefault('dino:server:docs', '/api-docs')
      router.use(docsPath, serve)
      router.use(docsPath, setup(apis, { explorer: false }))
      this.setupMonitoringEndpointsIfRequired(router)
      const monitoringMiddlewares = this.defineTopLevelMonitorsIfRequired()

      const errorHandler = this.errorHandler()
      const promises = Object.keys(apis.paths).map((path) =>
        this.configureApiPath(apis, path, apiValidator, monitoringMiddlewares, errorHandler, router)
      )
      await Promise.allSettled(promises)
    }
    this.eventProducer.send(ApplicationEvent.create('applicationStarted'))
    this.startServer(app, router)
  }

  public preDestroy() {
    if (ObjectHelper.isDefined(this.server)) {
      this.server.close()
    }
    this.eventProducer.send(ApplicationEvent.create('applicationStopped'))
  }

  private async configureApiPath(
    apis: OpenAPI.Document<{}>,
    path: string,
    apiValidator: ApiValidator,
    monitoringMiddlewares: OrderedMiddlewares,
    errorHandler: MiddlewareErrorHandler,
    router: Router
  ): Promise<void> {
    const currentPath = apis.paths![path]
    if (currentPath !== undefined) {
      const promises = Object.keys(currentPath).map((method) => {
        this.configureApiMethod(path, currentPath, method, apiValidator, apis, monitoringMiddlewares, errorHandler, router)
      })
      await Promise.allSettled(promises)
    }
  }

  private configureApiMethod(
    path: string,
    currentPath,
    method: string,
    apiValidator: ApiValidator,
    apis: OpenAPI.Document<{}>,
    monitoringMiddlewares: OrderedMiddlewares,
    errorHandler: MiddlewareErrorHandler,
    router: Router
  ) {
    const apiPath = Helper.normalizePath(path)
    const api = currentPath[method]
    const responseValidator = apiValidator.getResponseValidator(method, path)
    const requestHandler = this.requestHandler(api, responseValidator)

    // defines middlewares proposing built-ins and concatenating lesser ones
    // as per express style the user will be able to interrupt the chain just
    // sending a response, this at current stage is not a concern but a better
    // middleware structure that avoid this kind if hacks should be put in place.
    // this is tracked on https://gitlab.com/codesketch/dino-express/issues/7
    const userDefinedMiddlewares: OrderedMiddlewares = this.middlewares(api, apis) || { before: [], after: [] }
    const corsMiddleware: MiddlewareHandler = this.getCorsMiddlewareIfConfigured(api)
    if (corsMiddleware !== undefined) {
      // if cors is defined, enable the option method on the specific route
      router.options(apiPath, corsMiddleware as RequestHandler)
    }
    try {
      const requestValidator = apiValidator.getRequestValidator(method, path)
      const beforeMiddlewares: Middlewares = ([corsMiddleware, requestValidator] as Middlewares)
        .concat(userDefinedMiddlewares.before)
        .concat(monitoringMiddlewares.before)
      const afterMiddlewares: Middlewares = ([] as Middlewares)
        .concat(userDefinedMiddlewares.after)
        .concat(monitoringMiddlewares.after)
        .concat([new SendResponseMiddleware()])
      const middlewares = ([] as Middlewares)
        .concat(beforeMiddlewares)
        .concat([requestHandler, errorHandler])
        .concat(afterMiddlewares)
        .map((middleware) => {
          if (Helper.instanceOf(middleware, AfterHandlerMiddleware) || Helper.instanceOf(middleware, BeforeHandlerMiddleware)) {
            return (middleware as AfterHandlerMiddleware | BeforeHandlerMiddleware).handle.bind(middleware)
          }
          return middleware
        })

      router[method](apiPath, middlewares)
    } catch (e: any) {
      Logger.error(
        `unable to configure the router from the provided OpenAPI definition, ${e.type ?? ''} - ${e.name} - ${e.message}... stopping the application`
      )
      process.exit(1)
    }
  }

  protected startServer(app: Application, router: Router): void {
    app.use(this.environment.getOrDefault('dino:server:path', '/'), router)
    if (this.runtimeContext.isStandalone()) {
      const port = this.environment.getOrDefault('dino:server:port', 3030)
      const httpsConfig = this.environment.getOrDefault('dino:security:https', { enabled: false })
      if (httpsConfig.enabled) {
        this.server = https.createServer(httpsConfig.config, app).listen(port)
      } else {
        this.server = app.listen(port, () => {
          Logger.info(`application started, listening on port ${port}`)
        })
      }
    } else {
      // if serverless register the router so that it can be used at later stages
      this.applicationContext.add(ComponentDescriptor.createFromValue('dinoApiRouter', router, Scope.SINGLETON))
    }
  }

  protected resolveDescriptorProvider(): DescriptorProvider {
    let answer: DescriptorProvider
    try {
      Logger.info('try locate user defined descriptor provider from the application context')
      answer = this.applicationContext.resolve('descriptorProvider')
    } catch (e) {
      Logger.info('user defined descriptor provider not found using default file system based one')
      const oldStyle = this.environment.get('dino:descriptor:provider')
      const newStyle = this.environment.getOrDefault('dino:openapi:descriptor:provider', 'filesystem')

      const DescriptorProvider = DESCRIPTOR_PROVIDER_MAP[newStyle ?? oldStyle]
      answer = new DescriptorProvider(this.environment)
    }
    return answer
  }

  protected setupMonitoringEndpointsIfRequired(router: Router) {
    let monitoringEnabled: boolean = this.runtimeContext.isMonitoringEnabled()
    if (monitoringEnabled) {
      this.applicationStateMonitor = new ApplicationStateMonitor(this.applicationContext)
      router.use('/monitor', this.applicationStateMonitor.middleware())
    }
  }

  /**
   * Allows to define top level monitors. The returned objects contains two arrays
   * before and after middlewares, the before middlewares are added at the very start
   * of the middleware chain while the after are added at the very end of the middleware
   * chain, both in the order that are defined.
   *
   * @returns {Object}
   * ```{
   *    before: [],
   *    after: []
   *  }```
   */
  protected defineTopLevelMonitorsIfRequired(): OrderedMiddlewares {
    const answer: OrderedMiddlewares = { before: [], after: [] }
    let co2MonitoringEnabled = this.runtimeContext.isCo2MonitoringEnabled()
    if (co2MonitoringEnabled) {
      if (co2MonitoringEnabled) {
        const co2Monitor = new CO2EmissionMonitor()
        this.addMonitor(co2Monitor)
        answer.before.push(co2Monitor.requestMiddleware.bind(co2Monitor))
        answer.after.push(co2Monitor.responseMiddleware.bind(co2Monitor))
      }
    }
    return answer
  }

  /**
   * Allows to add a monitor
   * @param {Monitor} monitor
   */
  public addMonitor(monitor: Monitor): RoutingConfigurer {
    if (ObjectHelper.isDefined(this.applicationStateMonitor)) {
      this.applicationStateMonitor.addMonitor(monitor)
    }
    return this
  }

  /**
   * Hook method allow to customize the express instance. by default it add the json middleware only.
   * This method can be overwritten in order to provide more tailored configuration of express instance.
   *
   * @param express the express instance
   * @protected
   */
  protected extend(_express: Application) {}

  /**
   * Hook allows to provide an express error handler, by default a JSON error handler is used.
   * @protected
   */
  protected errorHandler(): MiddlewareErrorHandler {
    return ErrorHandler.instance(this.environment)
  }

  /**
   * Allows users to provide custom middlewares the middlewares will be inserted on the
   * chain as provided by the user but between the built-in API validation and route
   * handler and error handler.
   * @param {any} api the swagger API definition
   * @param {any} components the components OpenAPI definition
   * @returns a collection of middlewares split between before and after request handler
   *
   * @protected
   */
  protected middlewares(api: OpenAPI.Operation, components: OpenAPI.Document): OrderedMiddlewares {
    const policies: Middlewares = this.getPolicies(api['x-dino-express-policies'])
    return {
      before: policies.concat([
        new SecurityMiddleware(this.applicationContext, api, components),
        this.getCache(api, components),
        new ScopeCreatorMiddleware()
      ]),
      after: [new ProjectionsMiddleware()]
    }
  }

  /**
   * Hook allow to define a custom request handler middleware. If no middleware is
   * returned the default will be used.
   * @param {any} api the OpenAPI definition
   * @param {function} responseValidator the validator for the response expected by the OpenAPI definition.
   * @returns {Function}
   *
   * @protected
   */
  protected requestHandler(api: any, responseValidator: ResponseValidator): MiddlewareHandler {
    const routeHandler = new RouteHandler(this.applicationContext, api, responseValidator, this.environment, this.eventProducer)
    return routeHandler.handle.bind(routeHandler)
  }

  /**
   * Configures application observability events.
   * @returns the event producer
   */
  protected configureApplicationObservability(apiInfo: InfoObject): EventProducerInterface {
    let eventProducer: EventProducerInterface
    const config = this.environment.getOrDefault('dino:observability', { enabled: false, eventEmitter: '', batchSize: 1 })
    if (config.enabled) {
      let eventEmitter: EventEmitterInterface
      if (ObjectHelper.isDefined(config.eventEmitter)) {
        eventEmitter = this.applicationContext.resolve(config.eventEmitter)
      } else {
        eventEmitter = new LogEventEmitter()
      }
      const eventQueue = new EventQueue(eventEmitter, config.batchSize)
      eventProducer = new EventProducer(eventQueue, this.environment, apiInfo.title, apiInfo.version)
    }
    eventProducer = new NoopEventProducer()
    this.applicationContext.add(ComponentDescriptor.createFromValue('eventProducer', eventProducer, Scope.SINGLETON))
    return eventProducer
  }

  protected getCorsMiddlewareIfConfigured(api: OpenAPI.Operation): MiddlewareHandler {
    const corsConfiguration = api['x-dino-express-cors']
    if (ObjectHelper.isDefined(corsConfiguration)) {
      return cors(corsConfiguration)
    }
    return cors({ origin: false })
  }

  /**
   * Get the policies defined for this route
   * @param {Array<String>} policies the policies defined for this route
   * @returns {Array<AbstractPolicy>} the policies
   *
   * @private
   */
  protected getPolicies(policies: Policy[] = []): Middlewares {
    return policies.reduce((answer, policy) => {
      let _policy = this.policyFactory.getPolicy(policy.name)
      _policy.configure(policy.configuration)
      answer.push(_policy.middleware.bind(_policy))
      return answer
    }, [] as Middlewares)
  }

  /**
   * Get the cache middleware configured as requested for the current route
   * @param {any} cache the cache configuration
   * @returns {Function} the cache middleware.
   *
   * @private
   */
  protected getCache(api: any, _components): MiddlewareHandler {
    Logger.debug(`configuring cache for API ${api.operationId}`)
    let answer = (_req: Request, _res: Response, next: NextFunction) => next()
    const cache = api['x-dino-express-cache']
    if (cache !== undefined) {
      Logger.debug(`configuring cache ttl to ${cache.ttl}`)
      const cacheInstance = apicache.newInstance({
        debug: cache.debug || false,
        trackPerformance: cache.trackPerformance || true,
        isBypassable: cache.isBypassable || true
      })
      answer = cacheInstance.middleware(cache.ttl || '30 minutes', this.cacheOnlySuccessResponsesForGet)
      this.addMonitor(new CachePerformanceMonitor().setApiName(api.operationId).setCacheInstance(cacheInstance))
    }
    return answer
  }

  /**
   * Hook method that allows to define a global error handle, by default the
   * register global error handler will log the error and send an error event.
   *
   * @see {@link GlobalErrorHandler} for more info
   * @returns the error handler to register
   */
  protected getGlobalErrorHandler(): ErrorHandler {
    return new GlobalErrorHandler(this.eventProducer)
  }

  /**
   * Define a function that discriminate what responses should be cached. In this case only responses
   * with 200 response code from GET requests.
   * @returns {Function} the cache toggle function
   *
   * @private
   */
  private cacheOnlySuccessResponsesForGet(req: Request, res: Response): boolean {
    Logger.debug(`${res.statusCode} ${req.method}`)
    return res.statusCode == 200 && req.method.toLowerCase() == 'get'
  }
}
