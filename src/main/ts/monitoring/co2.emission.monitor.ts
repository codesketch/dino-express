//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Monitor, ObjectHelper, State } from 'dino-core'
import { co2 } from '@tgwf/co2'
import { type Request, type NextFunction } from 'express'
import { type Response } from '../Response'
import { ObjectSizeCalculator } from '../ObjectSizeCalculator'

export class CO2EmissionMonitor extends Monitor {
  private readonly properties: Map<string, any>
  private readonly co2EmissionCalculator: any
  private readonly objectSizeCalculator: ObjectSizeCalculator
  constructor() {
    super()
    this.objectSizeCalculator = new ObjectSizeCalculator()
    this.properties = new Map()
    this.properties.set('model', '1byte')
    this.properties.set('totalCO2GramsPerByte', 0)
    this.properties.set('requests', {
      total: 0,
      totalCO2GramsPerByteRead: 0
    })
    this.properties.set('responses', {
      total: 0,
      totalCO2GramsPerByteWritten: 0
    })
    // eslint-disable-next-line new-cap
    this.co2EmissionCalculator = new co2({ model: '1byte' })
  }

  public execute(): State {
    return State.from(this.properties)
  }

  public getName(): string {
    return 'CO2 Emissions'
  }

  public requestMiddleware(req: Request, _res: Response, next: NextFunction): void {
    this.collectRequestInfoRequired(req)
    next()
  }

  public responseMiddleware(_req: Request, res: Response, next: NextFunction): void {
    this.collectResponseInfoRequired(res)
    next()
  }

  private collectRequestInfoRequired(req: Request): void {
    const requests = this.properties.get('requests')
    const bytes = ObjectHelper.isDefined(req.socket) ? req.socket.bytesRead : this.objectSizeCalculator.calculateSize(req)
    const co2Emission = this.co2EmissionCalculator.perByte(bytes)
    requests.total += 1
    requests.totalCO2GramsPerByteRead += co2Emission
    this.properties.set('totalCO2GramsPerByte', this.properties.get('totalCO2GramsPerByte') + co2Emission)
  }

  private collectResponseInfoRequired(res: Response): void {
    const responses = this.properties.get('responses')
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const bytes = ObjectHelper.isDefined(res.socket) ? res.socket!.bytesWritten : this.objectSizeCalculator.calculateSize(res)
    const co2Emission = this.co2EmissionCalculator.perByte(bytes)
    responses.total += 1
    responses.totalCO2GramsPerByteWritten += co2Emission
    this.properties.set('totalCO2GramsPerByte', this.properties.get('totalCO2GramsPerByte') + co2Emission)
  }
}
