//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { type ApplicationContext, Monitor, ObjectHelper, type State } from 'dino-core'
import { type NextFunction, type Request, type Response } from 'express'

/**
 * The health monitor handler used to define the express error handling behaviour
 * @public
 */
export class ApplicationStateMonitor {
  private readonly monitors: Monitor[]
  private readonly applicationContext: ApplicationContext

  constructor(applicationContext: ApplicationContext) {
    this.monitors = []
    this.applicationContext = applicationContext
  }

  public doMonitor(res: Response): Response {
    const monitors = this.getApplicationStateMonitors()
    let properties = { default: [{ name: 'status', value: 'UP' }] }
    if (ObjectHelper.isDefined(monitors) && monitors.length > 0) {
      properties = monitors.reduce((answer, monitor) => {
        const state: State = monitor.execute()
        answer[monitor.getName()] = Array.from(state.getProperties()).map((entry: [string, any]) => {
          return { name: entry[0], value: entry[1] }
        })
        return answer
      }, properties)
    }
    return res.status(200).type('application/json').send(JSON.stringify(properties))
  }

  private getApplicationStateMonitors(): Monitor[] {
    if (ObjectHelper.isDefined(this.monitors)) {
      try {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
        const monitors: Monitor[] = this.applicationContext.resolveAll(Monitor) as Monitor[]
        this.monitors.concat(monitors)
      } catch (e) {
        // Ignore, there is no application state monitor defined, we'll use the default behaviour...
      }
    }
    return this.monitors
  }

  /**
   * Allows to add a monitor
   * @param {Monitor} monitor
   */
  public addMonitor(monitor: Monitor): ApplicationStateMonitor {
    this.monitors.push(monitor)
    return this
  }

  public middleware() {
    return (_req: Request, res: Response, _next: NextFunction) => this.doMonitor.bind(this)(res)
  }
}
