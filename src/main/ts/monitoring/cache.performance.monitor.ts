//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Monitor, State } from 'dino-core'

export class CachePerformanceMonitor extends Monitor {
  private apiName!: string
  private cacheInstance: any

  public execute(): State {
    const properties = new Map()
    const performances = this.cacheInstance.getPerformance()[0]
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    Object.keys(this.cacheInstance.getPerformance()[0]).forEach((key) => properties.set(key, performances[key]))
    return State.from(properties)
  }

  public getName(): string {
    return this.apiName
  }

  public setApiName(apiName: string): this {
    this.apiName = apiName
    return this
  }

  public setCacheInstance(cacheInstance: any): this {
    this.cacheInstance = cacheInstance
    return this
  }
}
