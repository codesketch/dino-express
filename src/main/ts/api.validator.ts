import { EnvironmentConfiguration } from 'dino-core';
import { NextFunction, Request, Response } from 'express';
import { OpenApiValidator } from 'express-openapi-validate';

export class ApiValidator {
  protected readonly validationEnabled: boolean;
  protected readonly environment: EnvironmentConfiguration;
  protected openApiValidator!: OpenApiValidator;

  constructor(environment) {
    this.environment = environment;
    this.validationEnabled = true;
    if (environment.get('dino:api:validation:enabled') != undefined) {
      this.validationEnabled = environment.get('dino:api:validation:enabled');
    }
  }

  init(apis) {
    if (this.validationEnabled) {
      const config = this.environment.get('dino:api:validation:configuration') || { ajvOptions: { coerceTypes: true } };
      this.openApiValidator = new OpenApiValidator(apis, config);
    }
  }

  getRequestValidator(method, path) {
    if (this.validationEnabled) {
      const validator = this.openApiValidator.validate(method, path);
      return (req: Request, res: Response, next: NextFunction) => {
        const reqToValidate = Object.assign({}, req, {
          cookies: req.cookies ? Object.assign({}, req.cookies, req.signedCookies) : undefined,
        });
        if (!reqToValidate.hasOwnProperty('headers')) {
          reqToValidate.headers = req.headers;
        }
        return validator(reqToValidate, res, next);
      };
    }
    return (_req: Request, _res: Response, next: NextFunction) => next();
  }

  getResponseValidator(method, path) {
    if (this.validationEnabled) {
      return this.openApiValidator.validateResponse(method, path);
    }
    return () => {};
  }
}
