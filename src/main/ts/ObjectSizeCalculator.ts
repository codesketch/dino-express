export class ObjectSizeCalculator {
  public calculateSize(object: any): number {
    const objectList: any[] = []
    const stack: any[] = [object]
    let bytes = 0

    while (stack.length > 0) {
      const value = stack.pop()

      if (typeof value === 'boolean') {
        bytes += 4
      } else if (typeof value === 'string') {
        bytes += value.length * 2
      } else if (typeof value === 'number') {
        bytes += 8
      } else if (typeof value === 'object' && !objectList.includes(value)) {
        objectList.push(value)
        // if the object is not an array, add the sizes of the keys
        if (Object.prototype.toString.call(value) !== '[object Array]') {
          for (const key in value) {
            bytes += 2 * key.length
          }
        }
        for (const key in value) {
          stack.push(value[key])
        }
      }
    }
    return bytes
  }
}
