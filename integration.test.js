//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global after */
/* global process */

const { Dino } = require('dino-core');
const assert = require('assert');
const request = require('supertest');
const { describe } = require('mocha');

describe.skip('Integration Test', async () => {
  process.env.DINO_CONTEXT_ROOT = 'examples/json';
  process.env.DINO_CONFIG_PATH = 'examples/json/config.json';
  const applicationContext = await Dino.run();
  const api = request('http://localhost:3030');

  after(function () {
    applicationContext.destroy();
  });

  it('throws error on invalid response', (done) => {
    api
      .get('/uuid/fail')
      .expect('Content-Type', /json/)
      .expect(400)
      .then((res) => {
        assert.ok(!!res.body.message);
        done();
      });
  });

  it('sends valid response', (done) => {
    api
      .get('/uuid')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((res) => {
        assert.ok(!!res.body.uuid);
        done();
      });
  });

  it('accepts boolean parameters as a query string', (done) => {
    api
      .get('/anynumber/1?flag=true')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((res) => {
        assert.ok(!!res.body.query.flag);
        done();
      });
  });
});
