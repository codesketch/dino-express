//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

const uuid = require('uuid/v4')

const Interface = require('../../../src/main/node/main').Interface
const Response = require('../../../src/main/node/main').Response

class MainInterface extends Interface {
  // eslint-disable-next-line no-unused-vars
  getUUID(req) {
    return Response.builder(200, { uuid: uuid() })
  }

  // eslint-disable-next-line no-unused-vars
  getAnything({ anything, flag, number, content_type, req }) {
    return Response.builder(200, { params: req.params, body: req.body, query: req.query }).setContentType('application/json')
  }

  project() {
    return 'X-Project'
  }
}

module.exports = MainInterface
